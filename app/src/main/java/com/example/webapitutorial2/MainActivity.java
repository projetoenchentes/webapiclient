package com.example.webapitutorial2;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {
    TextView textView;
    InputStream is;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("http://appenchentes.tk/api/event");

        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            textView = new TextView(this.getBaseContext());
            textView.setText(httpEntity.getContent().toString());
            Log.d("JSON", httpEntity.getContent().toString());
            is = httpEntity.getContent();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (ClientProtocolException e) {
//            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

//        String outPut = "";
//
//        try {
//            BufferedReader in = new BufferedReader(new InputStreamReader(
//                    is, "iso-8859-1"), 8);
//            StringBuilder sb = new StringBuilder();
//            String line = null;
//            while ((line = in.readLine()) != null) {
//                sb.append(line + "\n");
//            }
//            is.close();
//            outPut = sb.toString();
//            Log.e("JSON", outPut);
//        } catch (Exception e) {
//            Log.e("Buffer Error", "Error converting result " + e.toString());
//        }
//
//        JSONObject json = null;
//        try {
//            json = new JSONObject(outPut);
//        } catch (JSONException e) {
//            Log.e("JSON Parser", "Error parsing data " + e.toString());
//        }
//
//        // return JSON String
//        Log.d("Entrou", json.toString());
	}

}
